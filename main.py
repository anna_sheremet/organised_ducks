import pandas as pd

data = pd.read_excel('zadanie-rekrutacyjne.xlsx', header=None, names=["height", "width"])
df_data = pd.DataFrame(data)
amount_width = df_data.iloc[0]
amount_width.rename({'height': 'amount'}, inplace=True)
print(amount_width)

sorted_ducks = df_data.sort_values(by="height", ascending=False)
sorted_ducks = sorted_ducks.drop(axis=0, labels=0)
sorted_ducks = sorted_ducks.to_numpy()
print(sorted_ducks)


def max_height_in_row(n, m, ducks_array):
    heap = [(0, 0)]

    for i in range(n):
        new_heap = []

        for height, width in heap:
            new_heap.append((height + ducks_array[i][0], width + ducks_array[i][1]))
        new_heap.append((ducks_array[i][0], ducks_array[i][1]))
