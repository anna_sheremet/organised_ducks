import heapq
import pandas as pd


# function to maximize the sum of heights of ducks within width limit
def maximize_duck_heights(n, m, ducks):
    # sort ducks by decreasing height
    ducks = sorted(ducks, key=lambda x: x[0], reverse=True)

    # use a heap to keep track of the maximum heights that can be achieved with each width
    heap = [(0, 0)]  # (height, width)

    for i in range(n):
        new_heap = []
        for height, width in heap:
            if width + ducks[i][1] <= m:
                # if the current duck can fit in the current row, add it to the heap
                new_heap.append((height + ducks[i][0], width + ducks[i][1]))
        # add the current duck to the heap as a new row
        new_heap.append((ducks[i][0], ducks[i][1]))
        heap.extend(new_heap)
        # keep only the rows with total width less than or equal to m
        heap = [(height, width) for height, width in heap if width <= m]
        # keep only the rows with maximum height
        heap = heapq.nlargest(len(heap), heap)

    # return the maximum height achieved
    return heap[0][0]


data = pd.read_excel('zadanie-rekrutacyjne.xlsx', names=["height", "width"])
# example usage
ducks = data.to_numpy()
max_height = maximize_duck_heights(20, 50, ducks)
print("Maximum height:", max_height)
